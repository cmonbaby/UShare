package com.simon.umeng;

import android.app.Application;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * 设置组件化的Log开关
         * 参数: boolean 默认为false，如需查看LOG设置为true
         */
        UMConfigure.setLogEnabled(true);

        /**
         * 初始化common库
         * 参数1:上下文，不能为空
         * 参数2:【友盟+】 AppKey，非必须参数，如果Manifest文件中已配置AppKey，该参数可以传空
         * 参数3:【友盟+】 Channel，非必须参数，如果Manifest文件中已配置Channel，该参数可以传空
         * 参数4:设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
         * 参数5:Push推送业务的secret
         */
        UMConfigure.init(this, "5b471ec5a40fa312c1000037"
                , "link_car", UMConfigure.DEVICE_TYPE_PHONE, "a2c5b02c9d259d93805e40390ab0b63b");
    }

    /**
     * 静态块：AppID + APP Key（AppSecret）
     * 所需分享平台
     */
    {
        PlatformConfig.setWeixin("wx1adae26662887b1e", "65a8ac4d01aa74f87a89e5cfec304632");
        PlatformConfig.setQQZone("101485926", "5da885cd011f08358d455b5afa6b754f");
    }
}
