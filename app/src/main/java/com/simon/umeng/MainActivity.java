package com.simon.umeng;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.wx).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareUrl(SHARE_MEDIA.WEIXIN);
            }
        });

        findViewById(R.id.wx_quan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareUrl(SHARE_MEDIA.WEIXIN_CIRCLE);
            }
        });

        findViewById(R.id.qq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareUrl(SHARE_MEDIA.QQ);
            }
        });
    }

    public void shareUrl(SHARE_MEDIA type){
        UMWeb web = new UMWeb("http://www.cmonbaby.com");
        web.setTitle("标题：西门提督");
        web.setThumb(new UMImage(this, "http://cmonbaby.com/content/templates/ek_auto/style/images/qrcode.png"));
        web.setDescription("内容：推荐使用网络图片和资源文件的方式，平台兼容性更高。对于部分平台，分享的图片需要设置缩略图，缩略图的设置规则为");
        new ShareAction(MainActivity.this).withMedia(web )
                .setPlatform(type)
                .setCallback(shareListener).share();
    }

    private UMShareListener shareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA platform) {
        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            Toast.makeText(MainActivity.this,"成功了",Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            Toast.makeText(MainActivity.this,"失败"+t.getMessage(),Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Toast.makeText(MainActivity.this,"取消了",Toast.LENGTH_LONG).show();

        }
    };

    // QQ与新浪不需要添加Activity，但需要在使用QQ分享或者授权的Activity中，添加：
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

}
